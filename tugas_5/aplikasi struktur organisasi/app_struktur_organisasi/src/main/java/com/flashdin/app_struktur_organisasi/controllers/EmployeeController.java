package com.flashdin.app_struktur_organisasi.controllers;

import com.flashdin.app_struktur_organisasi.pojos.Company;
import com.flashdin.app_struktur_organisasi.pojos.Employee;
import com.flashdin.app_struktur_organisasi.services.CompanyService;
import com.flashdin.app_struktur_organisasi.services.EmployeeService;
import com.lowagie.text.*;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;

@Controller
@RequestMapping("/employee")
public class EmployeeController {

    @Autowired
    @Qualifier("employeeServiceImpl")
    private EmployeeService employeeService;

    @Autowired
    @Qualifier("companyServiceImpl")
    private CompanyService companyService;

    @GetMapping(path = "")
    public ModelAndView viewList(Model model, @RequestParam Map<String, Object> par) {
        ModelAndView mav = new ModelAndView("index");
        mav.addObject("title", "App Struktur Organisasi");
        mav.addObject("subtitle", "");
        mav.addObject("filterPg", "");
        Map<String, Object> map = par;
        map.put("jabatan", "ceo");
        List<Employee> emp = employeeService.findDataAll(map);
        map = new HashMap<>();
        List<Company> companies = companyService.findDataAll(map);
        mav.addObject("data", emp);
        mav.addObject("dataStruktur", this.genStrukturEmployee(emp));
        mav.addObject("dataCompany", companies);
        mav.addObject("res", model.getAttribute("res"));
        mav.addObject("mainPg", "employee/list");
        return mav;
    }

    @GetMapping(path = "/{id}")
    public ModelAndView viewFormAdd(@PathVariable(value = "id") long id) {
        ModelAndView mav = new ModelAndView("layout/content");
        mav.addObject("title", "App Struktur Organisasi");
        mav.addObject("subtitle", "");
        mav.addObject("filterPg", "");
        Map<String, Object> map = new HashMap<>();
        mav.addObject("companyOpt", companyService.findDataAll(map));
        map = new HashMap<>();
        map.put("jabatan", "ceo");
        mav.addObject("employeeOpt", employeeService.findDataAll(map));
        mav.addObject("employee", new Employee());
        if (id > 0) {
            Optional<Employee> companies = employeeService.findDataById(id);
            if (companies.isPresent()) {
                mav.addObject("employee", companies.get());
            }
        }
        mav.addObject("mainPg", "employee/add");
        return mav;
    }

    @PostMapping(path = "")
    public RedirectView saveData(@ModelAttribute Employee par, BindingResult bindingResult,
                                 RedirectAttributes redirectAttributes) {
        Map<String, Object> map = employeeService.saveData(par);
        RedirectView redirectView = new RedirectView();
        redirectView.setContextRelative(true);
        redirectView.setUrl("/employee");
        redirectAttributes.addFlashAttribute("res", map);
        return redirectView;
    }

    @PostMapping(path = "/del")
    public String deleteData(@ModelAttribute Employee par, RedirectAttributes redirectAttributes) {
        Map<String, Object> map = employeeService.deleteData(par);
        redirectAttributes.addFlashAttribute("res", map);
        return "redirect:/employee";
    }

    @GetMapping("/export/pdf")
    public void exportToPDF(HttpServletResponse response) throws DocumentException, IOException {
        response.setContentType("application/pdf");
        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=file.pdf";
        response.setHeader(headerKey, headerValue);
        Map<String, Object> map = new HashMap<>();
        map.put("jabatan", "ceo");
        List<Employee> emp = employeeService.findDataAll(map);
        String[] tblHeader = {"ID", "Nama", "Posisi", "Perusahaan"};
        List<Map<String, Object>> maps = new ArrayList<>();
        for (Employee employee : emp) {
            map = new HashMap<>();
            map.put("id", employee.getId());
            map.put("nama", employee.getNama());
            map.put("posisi", "CEO");
            map.put("perusahaan", employee.getCompany().getNama());
            maps.add(map);
        }
        for (Employee employee : emp) {
            for (Employee employee1 : employee.getEmployees()) {
                map = new HashMap<>();
                map.put("id", employee1.getId());
                map.put("nama", employee1.getNama());
                map.put("posisi", "Direktur");
                map.put("perusahaan", employee1.getCompany().getNama());
                maps.add(map);
            }
        }
        for (Employee employee : emp) {
            for (Employee employee1 : employee.getEmployees()) {
                for (Employee employee2 : employee1.getEmployees()) {
                    map = new HashMap<>();
                    map.put("id", employee2.getId());
                    map.put("nama", employee2.getNama());
                    map.put("posisi", "Manager");
                    map.put("perusahaan", employee2.getCompany().getNama());
                    maps.add(map);
                }
            }
        }
        for (Employee employee : emp) {
            for (Employee employee1 : employee.getEmployees()) {
                for (Employee employee2 : employee1.getEmployees()) {
                    for (Employee employee3 : employee2.getEmployees()) {
                        map = new HashMap<>();
                        map.put("id", employee3.getId());
                        map.put("nama", employee3.getNama());
                        map.put("posisi", "Staff");
                        map.put("perusahaan", employee3.getCompany().getNama());
                        maps.add(map);
                    }
                }
            }
        }
        Document document = new Document(PageSize.A4);
        PdfWriter.getInstance(document, response.getOutputStream());
        document.open();
        Paragraph p = new Paragraph("Data Employee");
        p.setAlignment(Paragraph.ALIGN_CENTER);
        document.add(p);
        PdfPTable table = new PdfPTable(tblHeader.length);
        table.setWidthPercentage(100f);
        table.setWidths(new float[]{1f, 3.0f, 3.0f, 3.0f});
        table.setSpacingBefore(10);
        PdfPCell cell = new PdfPCell();
        cell.setPadding(5);
        for (String str : tblHeader) {
            cell.setPhrase(new Phrase(str));
            table.addCell(cell);
        }
        for (Map<String, Object> map1 : maps) {
            table.addCell(String.valueOf(map1.get("id")));
            table.addCell(String.valueOf(map1.get("nama")));
            table.addCell(String.valueOf(map1.get("posisi")));
            table.addCell(String.valueOf(map1.get("perusahaan")));
        }
        document.add(table);
        document.close();
    }

    @GetMapping("/export/excel")
    public ResponseEntity<InputStreamResource> exportToExcel(HttpServletResponse response) {
        ByteArrayInputStream byteArrayInputStream = null;
        String[] tblHeader = {"ID", "Nama", "Posisi", "Perusahaan"};
        try (
                Workbook workbook = new XSSFWorkbook();
                ByteArrayOutputStream out = new ByteArrayOutputStream()
        ) {
            CreationHelper creationHelper = workbook.getCreationHelper();
            Sheet sheet = workbook.createSheet("dataPengajar");
            Font headerFont = workbook.createFont();
            headerFont.setBold(true);
            headerFont.setColor(IndexedColors.BLUE.getIndex());
            CellStyle headerCellStyle = workbook.createCellStyle();
            headerCellStyle.setFont(headerFont);
            //Row ofor Header
            Row headerRow = sheet.createRow(0);
            //Header
            for (int i = 0; i < tblHeader.length; i++) {
                Cell cell = headerRow.createCell(i);
                cell.setCellValue(tblHeader[i]);
                cell.setCellStyle(headerCellStyle);
            }
            int rowIdx = 1;
            Map<String, Object> map = new HashMap<>();
            map.put("jabatan", "ceo");
            List<Employee> emp = employeeService.findDataAll(map);
            List<Map<String, Object>> maps = new ArrayList<>();
            for (Employee employee : emp) {
                map = new HashMap<>();
                map.put("id", employee.getId());
                map.put("nama", employee.getNama());
                map.put("posisi", "CEO");
                map.put("perusahaan", employee.getCompany().getNama());
                maps.add(map);
            }
            for (Employee employee : emp) {
                for (Employee employee1 : employee.getEmployees()) {
                    map = new HashMap<>();
                    map.put("id", employee1.getId());
                    map.put("nama", employee1.getNama());
                    map.put("posisi", "Direktur");
                    map.put("perusahaan", employee1.getCompany().getNama());
                    maps.add(map);
                }
            }
            for (Employee employee : emp) {
                for (Employee employee1 : employee.getEmployees()) {
                    for (Employee employee2 : employee1.getEmployees()) {
                        map = new HashMap<>();
                        map.put("id", employee2.getId());
                        map.put("nama", employee2.getNama());
                        map.put("posisi", "Manager");
                        map.put("perusahaan", employee2.getCompany().getNama());
                        maps.add(map);
                    }
                }
            }
            for (Employee employee : emp) {
                for (Employee employee1 : employee.getEmployees()) {
                    for (Employee employee2 : employee1.getEmployees()) {
                        for (Employee employee3 : employee2.getEmployees()) {
                            map = new HashMap<>();
                            map.put("id", employee3.getId());
                            map.put("nama", employee3.getNama());
                            map.put("posisi", "Staff");
                            map.put("perusahaan", employee3.getCompany().getNama());
                            maps.add(map);
                        }
                    }
                }
            }
            for (Map<String, Object> map1 : maps) {
                Row row = sheet.createRow(rowIdx);
                Set<String> stringSet = map.keySet();
                row.createCell(0).setCellValue(String.valueOf(map1.get("id")));
                row.createCell(1).setCellValue(String.valueOf(map1.get("nama")));
                row.createCell(2).setCellValue(String.valueOf(map1.get("posisi")));
                row.createCell(3).setCellValue(String.valueOf(map1.get("perusahaan")));
                rowIdx++;
            }
            workbook.write(out);
            workbook.close();
            byteArrayInputStream = new ByteArrayInputStream(out.toByteArray());
        } catch (Exception e) {
            System.out.println(e);
        }
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "attachment; filename=file.xlsx");
        return ResponseEntity.ok()
                .headers(headers)
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .body(new InputStreamResource(byteArrayInputStream));
    }

    private String genStrukturEmployee(List<Employee> employees) {
        String str = "<div class='tree'>";
        int i0 = 0, i = 0, i1 = 0, i2 = 0;
        for (Employee employee : employees) {
            str += i0 == 0 ? "<ul>" : "";
            str += "<li>";
            str += "<a href='#'>" +
                    "<p><img src='http://localhost:8090/img/user_copy.jpg' class='w3-image w3-circle'></p>" +
                    "<p>" + employee.getNama() + " - CEO</p>" +
                    "<p>" + employee.getCompany().getNama() + "</p>" +
                    "</a>";
            str += i == 0 ? "<ul>" : "";
            for (Employee employee1 : employee.getEmployees()) {
                str += "<li>";
                str += "<a href='#'>" +
                        "<p><img src='http://localhost:8090/img/user_copy.jpg' class='w3-image w3-circle'></p>" +
                        "<p>" + employee1.getNama() + " - Direktur</p>" +
                        "<p>" + employee1.getCompany().getNama() + "</p>" +
                        "</a>";
                str += i1 == 0 ? "<ul>" : "";
                for (Employee employee2 : employee1.getEmployees()) {
                    str += "<li>";
                    str += "<a href='#'>" +
                            "<p><img src='http://localhost:8090/img/user_copy.jpg' class='w3-image w3-circle'></p>" +
                            "<p>" + employee2.getNama() + " - Manager</p>" +
                            "<p>" + employee2.getCompany().getNama() + "</p>" +
                            "</a>";
                    str += i2 == 0 ? "<ul>" : "";
                    for (Employee employee3 : employee2.getEmployees()) {
                        str += "<li>";
                        str += "<a href='#'>" +
                                "<p><img src='http://localhost:8090/img/user_copy.jpg' class='w3-image w3-circle'></p>" +
                                "<p>" + employee3.getNama() + " - Staff</p>" +
                                "<p>" + employee3.getCompany().getNama() + "</p>" +
                                "</a>";
                        str += "</li>";
                    }
                    str += i2 == 0 ? "</ul>" : "";
                    str += "</li>";
                    i2++;
                    if (i2 == employees.size()) {
                        i2 = 0;
                    }
                }
                str += i1 == 0 ? "</ul>" : "";
                str += "</li>";
                i1++;
                if (i1 == employees.size()) {
                    i1 = 0;
                }
            }
            str += i == 0 ? "</ul>" : "";
            str += "</li>";
            i++;
            if (i == employees.size()) {
                i = 0;
            }
        }
        str += i0 == 0 ? "</ul>" : "";
        i0++;
        if (i0 == employees.size()) {
            i0 = 0;
        }
        str += "</div>";
        return str;
    }

}

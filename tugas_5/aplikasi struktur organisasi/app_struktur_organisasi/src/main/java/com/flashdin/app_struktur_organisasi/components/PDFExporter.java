package com.flashdin.app_struktur_organisasi.components;

import com.lowagie.text.*;
import com.lowagie.text.Font;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class PDFExporter {

    private void writeTableHeader(PdfPTable table, String[] tblHeader) {
        PdfPCell cell = new PdfPCell();
        cell.setBackgroundColor(Color.BLUE);
        cell.setPadding(5);
        Font font = FontFactory.getFont(FontFactory.HELVETICA);
        font.setColor(Color.WHITE);
        for (String str : tblHeader) {
            cell.setPhrase(new Phrase(str, font));
            table.addCell(cell);
        }
    }

    private void writeTableData(PdfPTable table, List<Map<String, Object>> maps) {
        for (Map<String, Object> map : maps) {
            Set<String> stringSet = map.keySet();
            for (String key : stringSet) {
                table.addCell(String.valueOf(map.get(key)));
            }
        }
    }

    public void export(HttpServletResponse response, String[] tblHeader, List<Map<String, Object>> maps) throws DocumentException, IOException {
        Document document = new Document(PageSize.A4);
        PdfWriter.getInstance(document, response.getOutputStream());
        document.open();
        Font font = FontFactory.getFont(FontFactory.HELVETICA_BOLD);
        font.setSize(18);
        font.setColor(Color.BLUE);
        Paragraph p = new Paragraph("List of Users", font);
        p.setAlignment(Paragraph.ALIGN_CENTER);
        document.add(p);
        PdfPTable table = new PdfPTable(tblHeader.length);
        table.setWidthPercentage(100f);
//        table.setWidths(new float[]{1.5f, 3.5f, 3.0f, 3.0f, 1.5f});
        table.setSpacingBefore(10);
        writeTableHeader(table, tblHeader);
        writeTableData(table, maps);
        document.add(table);
        document.close();
    }
}

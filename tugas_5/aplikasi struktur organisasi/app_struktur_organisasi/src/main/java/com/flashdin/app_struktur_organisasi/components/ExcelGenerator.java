package com.flashdin.app_struktur_organisasi.components;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.*;

@Component
public class ExcelGenerator {

    /* export */
    public ByteArrayInputStream exportExcel(Map<String, Object> par) throws Exception {
        String[] columns = (String[]) par.get("columnName");
        try (
                Workbook workbook = new XSSFWorkbook();
                ByteArrayOutputStream out = new ByteArrayOutputStream()
        ) {
            CreationHelper creationHelper = workbook.getCreationHelper();
            Sheet sheet = workbook.createSheet(par.get("sheetName").toString());
            Font headerFont = workbook.createFont();
            headerFont.setBold(true);
            headerFont.setColor(IndexedColors.BLUE.getIndex());
            CellStyle headerCellStyle = workbook.createCellStyle();
            headerCellStyle.setFont(headerFont);
            //Row ofor Header
            Row headerRow = sheet.createRow(0);
            //Header
            for (int i = 0; i < columns.length; i++) {
                Cell cell = headerRow.createCell(i);
                cell.setCellValue(columns[i]);
                cell.setCellStyle(headerCellStyle);
            }
            int rowIdx = 1;
            List<Map<String, Object>> maps = (List<Map<String, Object>>) par.get("data");
            for (Map<String, Object> map : maps) {
                Row row = sheet.createRow(rowIdx);
                int i = 0;
                for (String key : map.keySet()) {
                    row.createCell(i).setCellValue(map.get(key).toString());
                    i++;
                }
                rowIdx++;
            }
            workbook.write(out);
            workbook.close();
            return new ByteArrayInputStream(out.toByteArray());
        } catch (Exception e) {
            return null;
        }
    }

    /* Import */
    public List<Map<String, Object>> importExcel(MultipartFile file, Map<String, Object> par) throws Exception {
        Workbook workbook = new XSSFWorkbook(file.getInputStream());
        Sheet sheet = workbook.getSheetAt(0);
        List<Map<String, Object>> maps = new ArrayList<>();
        for (int i = 0; i < (CoutRowExcel(sheet.rowIterator())); i++) {
            if (i == 0) {
                continue;
            }
            Row row = sheet.getRow(i);
            int j = 0;
            Map<String, Object> map = new HashMap<>();
            for (String key : par.keySet()) {
                map.put(key, row.getCell(j).getStringCellValue());
                j++;
            }
            maps.add(map);
        }
        return maps;
    }

    /* Cout Row of Excel Table */
    public static int CoutRowExcel(Iterator<Row> iterator) {
        int size = 0;
        while (iterator.hasNext()) {
            Row row = iterator.next();
            size++;
        }
        return size;
    }

}

package com.flashdin.app_struktur_organisasi.services.impl;

import com.flashdin.app_struktur_organisasi.models.CompanyRepository;
import com.flashdin.app_struktur_organisasi.pojos.Company;
import com.flashdin.app_struktur_organisasi.services.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class CompanyServiceImpl implements CompanyService {

    @Autowired
    private CompanyRepository companyRepository;

    @Override
    public List<Company> findDataAll(Map<String, Object> par) {
        return companyRepository.findDataAll(par);
    }

    @Override
    public Optional<Company> findDataById(long id) {
        return companyRepository.findDataById(id);
    }

    @Override
    public Map<String, Object> saveData(Company par) {
        return companyRepository.saveData(par);
    }

    @Override
    public Map<String, Object> deleteData(Company par) {
        return companyRepository.deleteData(par);
    }
}

package com.flashdin.app_struktur_organisasi.controllers;

import com.flashdin.app_struktur_organisasi.pojos.Company;
import com.flashdin.app_struktur_organisasi.services.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import java.util.*;

@Controller
@RequestMapping("/company")
public class CompanyController {

    @Autowired
    @Qualifier("companyServiceImpl")
    private CompanyService companyService;

    @GetMapping(path = "")
    public ModelAndView viewList(Model model, @RequestParam Map<String, Object> par) {
        ModelAndView mav = new ModelAndView("index");
        mav.addObject("title", "App Struktur Organisasi");
        mav.addObject("subtitle", "");
        mav.addObject("filterPg", "");
        Map<String, Object> map = par;
        List<Company> companies = companyService.findDataAll(map);
        mav.addObject("data", companies);
        mav.addObject("res", model.getAttribute("res"));
        mav.addObject("mainPg", "company/list");
        return mav;
    }

    @GetMapping(path = "/{id}")
    public ModelAndView viewFormAdd(@PathVariable(value = "id") long id) {
        ModelAndView mav = new ModelAndView("layout/content");
        mav.addObject("title", "App Struktur Organisasi");
        mav.addObject("subtitle", "");
        mav.addObject("filterPg", "");
        mav.addObject("company", new Company());
        if (id > 0) {
            Optional<Company> companies = companyService.findDataById(id);
            if (companies.isPresent()) {
                mav.addObject("company", companies.get());
            }
        }
        mav.addObject("mainPg", "company/add");
        return mav;
    }

    @PostMapping(path = "")
    public RedirectView saveData(@ModelAttribute Company par, BindingResult bindingResult,
                                 RedirectAttributes redirectAttributes) {
        Map<String, Object> map = companyService.saveData(par);
        RedirectView redirectView = new RedirectView();
        redirectView.setContextRelative(true);
        redirectView.setUrl("/company");
        redirectAttributes.addFlashAttribute("res", map);
        return redirectView;
    }

    @PostMapping(path = "/del")
    public String deleteData(@ModelAttribute Company par, RedirectAttributes redirectAttributes) {
        Map<String, Object> map = companyService.deleteData(par);
        redirectAttributes.addFlashAttribute("res", map);
        return "redirect:/company";
    }

}

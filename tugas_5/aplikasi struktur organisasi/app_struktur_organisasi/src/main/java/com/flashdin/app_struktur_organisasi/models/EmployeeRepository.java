package com.flashdin.app_struktur_organisasi.models;

import com.flashdin.app_struktur_organisasi.pojos.Employee;

import java.util.List;
import java.util.Map;

public interface EmployeeRepository extends BaseDao<Employee> {

    List<Employee> findDataByAtasanId(long id);
    List<Employee> findAllCEO(Map<String, Object> par);
    List<Employee> findAllDirektur();
    List<Employee> findAllManager();
    List<Employee> findAllStaff();
    List<Employee> findAllNotStaff();
    List<Employee> findAllNotMe(long id);

}

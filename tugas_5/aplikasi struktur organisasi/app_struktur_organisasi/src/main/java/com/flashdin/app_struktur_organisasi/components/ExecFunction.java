package com.flashdin.app_struktur_organisasi.components;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.Types;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Component
public class ExecFunction {

    private JdbcTemplate jdbcTemplate;

    public ExecFunction(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public Map<String, Object> execQueryInsert(String tbl, Map<String, Object> params, String msg) {
        String keys = "";
        String val = "";
        Set<String> stringSet = params.keySet();
        for (String key : stringSet) {
            keys += key + ",";
            val += "?,";
        }
        KeyHolder keyHolder = new GeneratedKeyHolder();
        String qry = "insert into " + tbl + "(" + keys.replaceAll(",$", "") + ")" + " values(" + val.replaceAll(",$", "") + ")";
        int rtn = jdbcTemplate.update(connection -> {
            PreparedStatement ps = connection.prepareStatement(qry, Statement.RETURN_GENERATED_KEYS);
            int i = 1;
            Object o;
            for (String key : stringSet) {
                o = params.get(key);
                if (o instanceof Integer || o instanceof Long) {
                    ps.setInt(i, Integer.valueOf(params.get(key).toString()));
                } else if (o instanceof Boolean) {
                    ps.setBoolean(i, Boolean.valueOf(params.get(key).toString()));
                } else if (o instanceof Float || o instanceof Double) {
                    ps.setDouble(i, Double.valueOf(params.get(key).toString()));
                } else if (o instanceof String) {
                    ps.setString(i, params.get(key).toString());
                } else {
                    ps.setNull(i, Types.INTEGER);
                }
                i++;
            }
            return ps;
        }, keyHolder);

        Map<String, Object> resMsg = new HashMap<>();
        switch (rtn) {
            case 0:
                resMsg.put("out", rtn);
                resMsg.put("msg", msg + "gagal disimpan");
                break;
            case 1:
                resMsg.put("out", rtn);
                resMsg.put("msg", msg + "berhasil disimpan");
                break;
        }
        return resMsg;
    }

    public Map<String, Object> execQueryUpdate(String tbl, Map<String, Object> params, Map<String, Object> where, String msg) {
        String keys = "";
        Set<String> stringSet = params.keySet();
        for (String key : stringSet) {
            keys += key + "=?,";
        }
        String keysWhere = "";
        Set<String> stringSetWhere = where.keySet();
        for (String key : stringSetWhere) {
            keysWhere += key + "=? and ";
        }
        KeyHolder keyHolder = new GeneratedKeyHolder();
        String qry = "update " + tbl + " set " + keys.replaceAll(",$", "") + " where " + keysWhere.replaceAll(" and $", "");
        int rtn = jdbcTemplate.update(connection -> {
            PreparedStatement ps = connection.prepareStatement(qry, Statement.RETURN_GENERATED_KEYS);
            int i = 1;
            Object o;
            for (String key : stringSet) {
                o = params.get(key);
                if (o instanceof Integer || o instanceof Long) {
                    ps.setInt(i, Integer.valueOf(params.get(key).toString()));
                } else if (o instanceof Boolean) {
                    ps.setBoolean(i, Boolean.valueOf(params.get(key).toString()));
                } else if (o instanceof Float || o instanceof Double) {
                    ps.setDouble(i, Double.valueOf(params.get(key).toString()));
                } else if (o instanceof String) {
                    ps.setString(i, params.get(key).toString());
                } else {
                    ps.setNull(i, Types.INTEGER);
                }
                i++;
            }
            for (String key : stringSetWhere) {
                o = where.get(key);
                if (o instanceof Integer || o instanceof Long) {
                    ps.setInt(i, Integer.valueOf(where.get(key).toString()));
                } else if (o instanceof Boolean) {
                    ps.setBoolean(i, Boolean.valueOf(where.get(key).toString()));
                } else if (o instanceof Float || o instanceof Double) {
                    ps.setDouble(i, Double.valueOf(where.get(key).toString()));
                } else {
                    ps.setString(i, where.get(key).toString());
                }
                i++;
            }
            return ps;
        }, keyHolder);
        Map<String, Object> resMsg = new HashMap<>();
        switch (rtn) {
            case 0:
                resMsg.put("out", rtn);
                resMsg.put("msg", msg + "gagal diubah");
                break;
            case 1:
                resMsg.put("out", rtn);
                resMsg.put("msg", msg + "berhasil diubah");
                break;
        }
        return resMsg;
    }

    public Map<String, Object> execQueryDelete(String tbl, Map<String, Object> params, String msg) {
        String keys = "";
        Set<String> stringSet = params.keySet();
        for (String key : stringSet) {
            keys += key + "=? and ";
        }
        KeyHolder keyHolder = new GeneratedKeyHolder();
        String qry = "delete from " + tbl + " where " + keys.replaceAll(" and $", "");
        int rtn = jdbcTemplate.update(connection -> {
            PreparedStatement ps = connection.prepareStatement(qry, Statement.RETURN_GENERATED_KEYS);
            int i = 1;
            Object o;
            for (String key : stringSet) {
                o = params.get(key);
                if (o instanceof Integer || o instanceof Long) {
                    ps.setInt(i, Integer.valueOf(params.get(key).toString()));
                } else if (o instanceof Boolean) {
                    ps.setBoolean(i, Boolean.valueOf(params.get(key).toString()));
                } else if (o instanceof Float || o instanceof Double) {
                    ps.setDouble(i, Double.valueOf(params.get(key).toString()));
                } else {
                    ps.setString(i, params.get(key).toString());
                }
                i++;
            }
            return ps;
        }, keyHolder);
        Map<String, Object> resMsg = new HashMap<>();
        switch (rtn) {
            case 0:
                resMsg.put("out", rtn);
                resMsg.put("msg", msg + "gagal dihapus");
                break;
            case 1:
                resMsg.put("out", rtn);
                resMsg.put("msg", msg + "berhasil dihapus");
                break;
        }
        return resMsg;
    }

    public List<Map<String, Object>> execQuerySelect(Map<String, Object> params) {
        String sql = params.get("query").toString();
        List<Map<String, Object>> ls = jdbcTemplate.queryForList(sql);
        return ls;
    }

}

package com.flashdin.app_struktur_organisasi.models;

import com.flashdin.app_struktur_organisasi.pojos.Company;

public interface CompanyRepository extends BaseDao<Company> {
}

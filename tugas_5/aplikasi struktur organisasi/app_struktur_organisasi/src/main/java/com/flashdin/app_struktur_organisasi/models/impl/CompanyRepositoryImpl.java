package com.flashdin.app_struktur_organisasi.models.impl;

import com.flashdin.app_struktur_organisasi.components.ExecFunction;
import com.flashdin.app_struktur_organisasi.models.CompanyRepository;
import com.flashdin.app_struktur_organisasi.pojos.Company;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Repository
public class CompanyRepositoryImpl implements CompanyRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public List<Company> findDataAll(Map<String, Object> par) {
        String query = "select * from company";
        List<Company> ls = jdbcTemplate.query(query, new BeanPropertyRowMapper<>(Company.class));
        return ls;
    }

    @Override
    public Optional<Company> findDataById(long id) {
        String query = "select * from company where id=?";
        Company company = jdbcTemplate.queryForObject(query, new Object[]{id}, new BeanPropertyRowMapper<>(Company.class));
        Optional<Company> companyOptional = Optional.ofNullable(company);
        return companyOptional;
    }

    @Override
    public Map<String, Object> saveData(Company par) {
        Map<String, Object> res = new HashMap<>();
        Map<String, Object> map = new HashMap<>();
        map.put("nama", par.getNama());
        map.put("alamat", par.getAlamat());
        ExecFunction execFunction = new ExecFunction(this.jdbcTemplate);
        if (par.getId() == 0) {
            String sql = "select coalesce(max(id), 0) as id from company";
            Company company = jdbcTemplate.queryForObject(
                    sql, new BeanPropertyRowMapper<>(Company.class));
            map.put("id", company.getId() + 1);
            res = execFunction.execQueryInsert("company", map, "Data");
        } else {
            Map<String, Object> mapWhere = new HashMap<>();
            mapWhere.put("id", par.getId());
            res = execFunction.execQueryUpdate("company", map, mapWhere, "Data");
        }
        return res;
    }

    @Override
    public Map<String, Object> deleteData(Company par) {
        Map<String, Object> map = new HashMap<>();
        map.put("id", par.getId());
        return new ExecFunction(this.jdbcTemplate).execQueryDelete("company", map, "Data");
    }
}

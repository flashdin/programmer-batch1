package com.flashdin.app_struktur_organisasi.controllers;

import com.flashdin.app_struktur_organisasi.pojos.Employee;
import com.flashdin.app_struktur_organisasi.services.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/employee")
public class EmployeeRest {

    @Autowired
    @Qualifier("employeeServiceImpl")
    private EmployeeService employeeService;

    @GetMapping(path = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Employee> findDataAll(@RequestParam Map<String, Object> par) {
        return employeeService.findDataAll(par);
    }


}

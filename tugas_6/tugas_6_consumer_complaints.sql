-- Jumlah komplain setiap bulan
select count(complaint_id) as complaint_by_month
from ConsumerComplaints
group by month(date_received);

-- Komplain yang memiliki tags ‘Older American’
select count(complaint_id) as complaint_by_tags
from ConsumerComplaints
where tags = 'Older American';

-- Buat sebuah view yang menampilkan data nama perusahaan, jumlah company response to consumer seperti tabel di bawah
create or replace view vw_complaints as
select c.company,
       count(c1.complaint_id) as closed,
       count(c2.complaint_id) as closed_with_explanation,
       count(c3.complaint_id) as closed_with_non_monetary
from ConsumerComplaints c
       left join ConsumerComplaints c1 on c.complaint_id = c1.complaint_id and c.company_response_to_consumer = 'Closed'
       left join ConsumerComplaints c2
                 on c.complaint_id = c2.complaint_id and c.company_response_to_consumer = 'Closed with explanation'
       left join ConsumerComplaints c3 on c.complaint_id = c3.complaint_id and
                                          c.company_response_to_consumer = 'Closed with non-monetary relief'
group by c.company;

-- select view
select *
from vw_complaints where company = 'Wells Fargo & Company';
package com.flashdin.sikampus.models;

import com.flashdin.sikampus.pojos.Mahasiswa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface MahasiswaRepository extends JpaRepository<Mahasiswa, Long>, JpaSpecificationExecutor<Mahasiswa> {

}

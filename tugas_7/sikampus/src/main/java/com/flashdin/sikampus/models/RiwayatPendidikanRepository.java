package com.flashdin.sikampus.models;

import com.flashdin.sikampus.pojos.RiwayatPendidikan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RiwayatPendidikanRepository extends JpaRepository<RiwayatPendidikan, Long>, JpaSpecificationExecutor<RiwayatPendidikan> {

    List<RiwayatPendidikan> findAllByDosenIdDosenAndIdRiwayatNotIn(long idDosen, List<Long> idRiwayat);

}

package com.flashdin.sikampus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SikampusApplication {

	public static void main(String[] args) {
		SpringApplication.run(SikampusApplication.class, args);
	}

}

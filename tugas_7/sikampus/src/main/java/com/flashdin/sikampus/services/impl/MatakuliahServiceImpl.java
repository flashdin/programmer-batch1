package com.flashdin.sikampus.services.impl;

import com.flashdin.sikampus.models.MatakuliahRepository;
import com.flashdin.sikampus.pojos.Matakuliah;
import com.flashdin.sikampus.services.MatakuliahService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.*;

@Service
public class MatakuliahServiceImpl implements MatakuliahService {

    @Autowired
    private MatakuliahRepository matakuliahRepository;

    @Override
    public Page<Matakuliah> findDataAll(Map<String, Object> par) {
        String parSort = par.get("sort") == null ? "idMatakuliah" : par.get("sort").toString();
        String order = par.get("order") == null ? "asc" : par.get("order").toString();
        int pg = par.get("page") == null ? 0 : Integer.parseInt(par.get("page").toString()) - 1;
        int size = par.get("size") == null ? 10 : Integer.parseInt(par.get("size").toString());
        Sort sort = Sort.by(parSort);
        if (order.equals("asc")) {
            sort = sort.ascending();
        } else {
            sort = sort.descending();
        }
        Pageable pageable = PageRequest.of(pg, size, sort);
        Page<Matakuliah> page = null;
        if (par.get("keyword") == null) {
            page = matakuliahRepository.findAll(pageable);
        } else {
            pageable = PageRequest.of(pg, size, sort);
            page = matakuliahRepository.findAll(new Specification<Matakuliah>() {
                @Override
                public Predicate toPredicate(Root<Matakuliah> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
                    List<Predicate> predicates = new ArrayList<>();
                    predicates.add(criteriaBuilder.like(root.get("nmMatakuliah"), "%" + par.get("keyword").toString() + "%"));
                    return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
                }
            }, pageable);
        }
        return page;
    }

    @Override
    public Optional<Matakuliah> findDataById(long id) {
        return matakuliahRepository.findById(id);
    }

    @Override
    public Map<String, Object> saveData(Matakuliah par) {
        Map<String, Object> map = new HashMap<>();
        Matakuliah matakuliah = matakuliahRepository.save(par);
        if (matakuliah.getIdMatakuliah() > 0) {
            map.put("out", "1");
            map.put("msg", "Data berhasil disimpan");
        } else {
            map.put("out", "0");
            map.put("msg", "Data gagal disimpan");
        }
        return map;
    }

    @Override
    public Map<String, Object> deleteData(Matakuliah par) {
        Map<String, Object> map = new HashMap<>();
        matakuliahRepository.delete(par);
        long cn = matakuliahRepository.count(Example.of(par));
        if (cn <= 0) {
            map.put("out", "1");
            map.put("msg", "Data berhasil dihapus");
        } else {
            map.put("out", "0");
            map.put("msg", "Data gagal dihapus");
        }
        return map;
    }
}

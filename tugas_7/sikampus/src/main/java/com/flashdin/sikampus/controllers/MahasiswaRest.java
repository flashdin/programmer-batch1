package com.flashdin.sikampus.controllers;

import com.flashdin.sikampus.pojos.Mahasiswa;
import com.flashdin.sikampus.services.MahasiswaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping(path = "/api/mahasiswa")
public class MahasiswaRest {

    @Autowired
    @Qualifier("mahasiswaServiceImpl")
    private MahasiswaService mahasiswaService;

    @GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Optional<Mahasiswa> findDataById(@PathVariable(name = "id") long id) {
        return mahasiswaService.findDataById(id);
    }

}

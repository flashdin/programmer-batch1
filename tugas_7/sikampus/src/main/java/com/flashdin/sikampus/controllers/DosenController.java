package com.flashdin.sikampus.controllers;

import com.flashdin.sikampus.pojos.Dosen;
import com.flashdin.sikampus.services.DosenService;
import com.flashdin.sikampus.services.MatakuliahService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Controller
@RequestMapping("/dosen")
public class DosenController {

    @Autowired
    @Qualifier("dosenServiceImpl")
    private DosenService dosenService;
    @Autowired
    @Qualifier("matakuliahServiceImpl")
    private MatakuliahService matakuliahService;

    @GetMapping(path = "")
    public ModelAndView viewList(Model model, @RequestParam Map<String, Object> par) {
        ModelAndView mav = new ModelAndView("index");
        mav.addObject("title", "App Sikampus");
        mav.addObject("subtitle", "");
        mav.addObject("filterPg", "");
        Map<String, Object> map = par;
        Page<Dosen> dosens = dosenService.findDataAll(map);
        mav.addObject("data", dosens);
        mav.addObject("res", model.getAttribute("res"));
        mav.addObject("mainPg", "dosen/list");
        return mav;
    }

    @GetMapping(path = "/riwayat")
    public ModelAndView viewRiwayat(Model model) {
        ModelAndView mav = new ModelAndView("layout/content");
        mav.addObject("title", "App Sikampus");
        mav.addObject("subtitle", "");
        mav.addObject("filterPg", "");
        mav.addObject("mainPg", "dosen/riwayat");
        return mav;
    }

    @GetMapping(path = "/{id}")
    public ModelAndView viewFormAdd(@PathVariable(value = "id") long id) {
        ModelAndView mav = new ModelAndView("layout/content");
        mav.addObject("title", "App Sikampus");
        mav.addObject("subtitle", "");
        mav.addObject("filterPg", "");
        Dosen dosen = new Dosen();
        mav.addObject("dosen", dosen);
        if (id > 0) {
            Optional<Dosen> optionalDosen = dosenService.findDataById(id);
            if (optionalDosen.isPresent()) {
                mav.addObject("dosen", optionalDosen.get());
            }
        }
        mav.addObject("mainPg", "dosen/add");
        return mav;
    }

    @GetMapping(path = "/matakuliah/{id}")
    public ModelAndView viewFormAddMatakuliah(@PathVariable(value = "id") long id) {
        ModelAndView mav = new ModelAndView("layout/content");
        mav.addObject("title", "App Sikampus");
        mav.addObject("subtitle", "");
        mav.addObject("filterPg", "");
        Dosen dosen = new Dosen();
        dosen.setIdDosen(id);
        mav.addObject("dosen", dosen);
        Map<String, Object> map = new HashMap<>();
        map.put("size", 200);
        mav.addObject("matakuliahs", matakuliahService.findDataAll(map));
        mav.addObject("mainPg", "dosen/matakuliah");
        return mav;
    }

    @PostMapping(path = "")
    public RedirectView saveData(@ModelAttribute Dosen par, BindingResult bindingResult,
                                 RedirectAttributes redirectAttributes) {
        Map<String, Object> map = dosenService.saveData(par);
        RedirectView redirectView = new RedirectView();
        redirectView.setContextRelative(true);
        redirectView.setUrl("/dosen");
        redirectAttributes.addFlashAttribute("res", map);
        return redirectView;
    }

    @PostMapping(path = "/del")
    public String deleteData(@ModelAttribute Dosen par, RedirectAttributes redirectAttributes) {
        Map<String, Object> map = dosenService.deleteData(par);
        redirectAttributes.addFlashAttribute("res", map);
        return "redirect:/dosen";
    }

}

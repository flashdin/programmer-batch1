package com.flashdin.sikampus.pojos;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "dosen", schema = "si_kampus")
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "idDosen")
@Getter
@Setter
public class Dosen {

    @Id
    @Column(columnDefinition = "int(11)")
    private long idDosen;
    @Column(columnDefinition = "varchar(50)")
    private String namaDosen;
    @Column(columnDefinition = "char(13)", unique = true)
    private String nip;
    @Column(columnDefinition = "varchar(10)")
    private String gelar;

    @OneToMany(mappedBy = "dosen", cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH}, targetEntity = RiwayatPendidikan.class)
    private List<RiwayatPendidikan> riwayatPendidikans;

    @ManyToMany(targetEntity = Matakuliah.class, mappedBy = "dosens", cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
    private List<Matakuliah> matakuliahs;

}

package com.flashdin.sikampus.pojos.embeds;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import java.io.Serializable;

@Embeddable
@Getter
@Setter
public class MahasiswaMatakuliahIdentity implements Serializable {

    @Column(columnDefinition = "int(11)")
    @JoinColumn(name = "id_mahasiswa", referencedColumnName = "idMahasiswa", nullable = true)
    private long idMahasiswa;
    @Column(columnDefinition = "int(11)")
    @JoinColumn(name = "id_matakuliah", referencedColumnName = "idMatakuliah", nullable = true)
    private long idMatakuliah;
}

package com.flashdin.sikampus.controllers;

import com.flashdin.sikampus.pojos.Matakuliah;
import com.flashdin.sikampus.services.MatakuliahService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/api/matakuliah")
public class MatakuliahRest {

    @Autowired
    @Qualifier("matakuliahServiceImpl")
    private MatakuliahService matakuliahService;

    @PostMapping(path = "/dosen", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> saveData(@RequestBody Matakuliah par) {
        return ResponseEntity.ok(matakuliahService.saveData(par));
    }

}

package com.flashdin.sikampus.pojos;

import com.flashdin.sikampus.pojos.embeds.MahasiswaMatakuliahIdentity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "kelas", schema = "si_kampus")
@Getter
@Setter
public class Kelas {

    @EmbeddedId
    private MahasiswaMatakuliahIdentity mahasiswaMatakuliahIdentity;

    @Column(columnDefinition = "varchar(50)")
    private String namaKelas;

    @MapsId("idMahasiswa")
    @ManyToOne
    private Mahasiswa mahasiswa;
    @MapsId("idMatakuliah")
    @ManyToOne
    private Matakuliah matakuliah;

}


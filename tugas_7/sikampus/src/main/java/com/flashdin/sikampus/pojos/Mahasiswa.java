package com.flashdin.sikampus.pojos;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "mahasiswa", schema = "si_kampus")
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "idMahasiswa")
@Getter
@Setter
public class Mahasiswa {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "int(11)")
    private long idMahasiswa;
    @Column(columnDefinition = "varchar(50)")
    private String namaMahasiswa;
    @Column(columnDefinition = "char(13)", unique = true)
    private String nim;
    @Column(columnDefinition = "char(7)")
    private String jnsKelamin;
    @Column(columnDefinition = "varchar(50)")
    private String tmpLahir;
    @Column(columnDefinition = "date")
    @JsonFormat(pattern="yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date tglLahir;

    @OneToMany(mappedBy = "mahasiswa", cascade = CascadeType.ALL, targetEntity = Kelas.class)
    private List<Kelas> kelass;

}

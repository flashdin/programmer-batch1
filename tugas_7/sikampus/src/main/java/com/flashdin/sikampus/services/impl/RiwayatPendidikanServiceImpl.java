package com.flashdin.sikampus.services.impl;

import com.flashdin.sikampus.models.RiwayatPendidikanRepository;
import com.flashdin.sikampus.pojos.RiwayatPendidikan;
import com.flashdin.sikampus.services.RiwayatPendidikanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.*;

@Service
public class RiwayatPendidikanServiceImpl implements RiwayatPendidikanService {

    @Autowired
    private RiwayatPendidikanRepository riwayatPendidikanRepository;

    @Override
    public Page<RiwayatPendidikan> findDataAll(Map<String, Object> par) {
        String parSort = par.get("sort") == null ? "idRiwayat" : par.get("sort").toString();
        String order = par.get("order") == null ? "asc" : par.get("order").toString();
        int pg = par.get("page") == null ? 0 : Integer.parseInt(par.get("page").toString()) - 1;
        int size = par.get("size") == null ? 10 : Integer.parseInt(par.get("size").toString());
        Sort sort = Sort.by(parSort);
        if (order.equals("asc")) {
            sort = sort.ascending();
        } else {
            sort = sort.descending();
        }
        Pageable pageable = PageRequest.of(pg, size, sort);
        Page<RiwayatPendidikan> page = null;
        if (par.get("keyword") == null) {
            page = riwayatPendidikanRepository.findAll(pageable);
        } else {
            pageable = PageRequest.of(pg, size, sort);
            page = riwayatPendidikanRepository.findAll(new Specification<RiwayatPendidikan>() {
                @Override
                public Predicate toPredicate(Root<RiwayatPendidikan> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
                    List<Predicate> predicates = new ArrayList<>();
                    predicates.add(criteriaBuilder.like(root.get("nmRiwayatPendidikan"), "%" + par.get("keyword").toString() + "%"));
                    return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
                }
            }, pageable);
        }
        return page;
    }

    @Override
    public Optional<RiwayatPendidikan> findDataById(long id) {
        return riwayatPendidikanRepository.findById(id);
    }

    @Override
    public Map<String, Object> saveData(RiwayatPendidikan par) {
        Map<String, Object> map = new HashMap<>();
        RiwayatPendidikan riwayatPendidikan = riwayatPendidikanRepository.save(par);
        if (riwayatPendidikan.getIdRiwayat() > 0) {
            map.put("out", "1");
            map.put("msg", "Data berhasil disimpan");
        } else {
            map.put("out", "0");
            map.put("msg", "Data gagal disimpan");
        }
        return map;
    }

    @Override
    public Map<String, Object> deleteData(RiwayatPendidikan par) {
        Map<String, Object> map = new HashMap<>();
        riwayatPendidikanRepository.delete(par);
        long cn = riwayatPendidikanRepository.count(Example.of(par));
        if (cn <= 0) {
            map.put("out", "1");
            map.put("msg", "Data berhasil dihapus");
        } else {
            map.put("out", "0");
            map.put("msg", "Data gagal dihapus");
        }
        return map;
    }
}

package com.flashdin.sikampus.controllers;

import com.flashdin.sikampus.pojos.Mahasiswa;
import com.flashdin.sikampus.services.MahasiswaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import java.util.Map;
import java.util.Optional;

@Controller
@RequestMapping("/mahasiswa")
public class MahasiswaController {

    @Autowired
    @Qualifier("mahasiswaServiceImpl")
    private MahasiswaService mahasiswaService;

    @GetMapping(path = "")
    public ModelAndView viewList(Model model, @RequestParam Map<String, Object> par) {
        ModelAndView mav = new ModelAndView("index");
        mav.addObject("title", "App Sikampus");
        mav.addObject("subtitle", "");
        mav.addObject("filterPg", "");
        Map<String, Object> map = par;
        Page<Mahasiswa> mahasiswas = mahasiswaService.findDataAll(map);
        mav.addObject("data", mahasiswas);
        mav.addObject("res", model.getAttribute("res"));
        mav.addObject("mainPg", "mahasiswa/list");
        return mav;
    }

    @GetMapping(path = "/{id}")
    public ModelAndView viewFormAdd(@PathVariable(value = "id") long id) {
        ModelAndView mav = new ModelAndView("layout/content");
        mav.addObject("title", "App Sikampus");
        mav.addObject("subtitle", "");
        mav.addObject("filterPg", "");
        Mahasiswa mahasiswa = new Mahasiswa();
        mav.addObject("mahasiswa", mahasiswa);
        if (id > 0) {
            Optional<Mahasiswa> optionalMahasiswa = mahasiswaService.findDataById(id);
            if (optionalMahasiswa.isPresent()) {
                mav.addObject("mahasiswa", optionalMahasiswa.get());
            }
        }
        mav.addObject("mainPg", "mahasiswa/add");
        return mav;
    }

    @PostMapping(path = "")
    public RedirectView saveData(@ModelAttribute Mahasiswa par, BindingResult bindingResult,
                                 RedirectAttributes redirectAttributes) {
        Map<String, Object> map = mahasiswaService.saveData(par);
        RedirectView redirectView = new RedirectView();
        redirectView.setContextRelative(true);
        redirectView.setUrl("/mahasiswa");
        redirectAttributes.addFlashAttribute("res", map);
        return redirectView;
    }

    @PostMapping(path = "/del")
    public String deleteData(@ModelAttribute Mahasiswa par, RedirectAttributes redirectAttributes) {
        Map<String, Object> map = mahasiswaService.deleteData(par);
        redirectAttributes.addFlashAttribute("res", map);
        return "redirect:/mahasiswa";
    }

}

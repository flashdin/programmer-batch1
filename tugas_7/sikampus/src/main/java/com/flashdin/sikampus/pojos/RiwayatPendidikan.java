package com.flashdin.sikampus.pojos;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "riwayat_pendidikan", schema = "si_kampus")
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "idRiwayat")
@Getter
@Setter
public class RiwayatPendidikan {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "int(11)")
    private long idRiwayat;
    @Column(columnDefinition = "varchar(10)")
    private String strata;
    @Column(columnDefinition = "varchar(50)")
    private String jurusan;
    @Column(columnDefinition = "varchar(50)")
    private String sekolah;
    @Column(columnDefinition = "char(5)")
    private String tahunMulai;
    @Column(columnDefinition = "char(5)")
    private String tahunSelesai;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "id_dosen", referencedColumnName = "idDosen", nullable = true)
    private Dosen dosen;

}

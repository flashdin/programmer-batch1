package com.flashdin.sikampus.services;

import com.flashdin.sikampus.pojos.Kelas;

import java.util.List;
import java.util.Map;

public interface KelasService extends BaseService<Kelas> {

    Map<String, Object> saveData(List<Kelas> par);

}

package com.flashdin.sikampus.models;

import com.flashdin.sikampus.pojos.Dosen;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface DosenRepository extends JpaRepository<Dosen, Long>, JpaSpecificationExecutor<Dosen> {

    @Query(nativeQuery = true, value = "select coalesce(max(id_dosen), 0) from dosen")
    long getMaxId();

}

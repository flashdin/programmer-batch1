package com.flashdin.sikampus.services;

import com.flashdin.sikampus.pojos.Dosen;

import java.util.Map;

public interface DosenService extends BaseService<Dosen> {

    Map<String, Object> saveDataMatakuliah(Dosen par);
}

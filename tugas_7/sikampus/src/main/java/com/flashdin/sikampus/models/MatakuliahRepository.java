package com.flashdin.sikampus.models;

import com.flashdin.sikampus.pojos.Matakuliah;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface MatakuliahRepository extends JpaRepository<Matakuliah, Long>, JpaSpecificationExecutor<Matakuliah> {

}

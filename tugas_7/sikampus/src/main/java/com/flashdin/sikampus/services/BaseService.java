package com.flashdin.sikampus.services;

import org.springframework.data.domain.Page;

import java.util.Map;
import java.util.Optional;

public interface BaseService<T> {

    Page<T> findDataAll(Map<String, Object> par);
    Optional<T> findDataById(long id);
    Map<String, Object> saveData(T par);
    Map<String, Object> deleteData(T par);
}

package com.flashdin.sikampus.pojos;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "matakuliah", schema = "si_kampus")
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "idMatakuliah")
@Getter
@Setter
public class Matakuliah {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "int(11)")
    private long idMatakuliah;
    @Column(columnDefinition = "varchar(50)")
    private String namaMatakuliah;
    @Column(columnDefinition = "int(5)")
    private String jmlSks;

    @ManyToMany(targetEntity = Dosen.class, cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinTable(
            name = "dosenMatakuliah",
            joinColumns = @JoinColumn(name = "id_matakuliah", referencedColumnName = "idMatakuliah"),
            inverseJoinColumns = @JoinColumn(name = "id_dosen", referencedColumnName = "idDosen"))
    private List<Dosen> dosens;

    @OneToMany(mappedBy = "mahasiswa", cascade = CascadeType.ALL, targetEntity = Kelas.class)
    private List<Kelas> kelass;

}

package com.flashdin.sikampus.services;

import com.flashdin.sikampus.pojos.Mahasiswa;

public interface MahasiswaService extends BaseService<Mahasiswa> {
}

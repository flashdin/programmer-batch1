# CEO
select *
from employee
where atasan_id is null;
# staf biasa
select e.*
from employee e
where e.id not in (select atasan_id from employee where atasan_id is not null);
# direktur
select e.*
from employee e
       join employee e2 on e.atasan_id = e2.id
where e2.atasan_id is null;
# manager
select *
from employee
where atasan_id in
      (select e.id
       from employee e
              join employee e2 on e.atasan_id = e2.id
       where e2.atasan_id is null)
  and id not in (select id
                 from employee
                 where id not in (select distinct atasan_id from employee where atasan_id is not null));
# bawahan parameter nama
select (select (count(distinct e1.id) + count(distinct e2.id) + count(distinct e3.id))
        from employee e1
               left join employee e2 on e2.atasan_id = e1.id
               left join employee e3 on e3.atasan_id = e2.id
        where e1.atasan_id = e.id) as jumlah_bawahan
from employee e
where e.nama like 'pak budi';
package file;

import java.util.*;

public class KalkulatorSederhana {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Enter String: ");
        String in = input.nextLine();
        kalkulator(in);
        input.close();
    }

    private static void kalkulator(String in) {
        String inp = in.replaceAll(" ", "");
        String[] num = inp.split("[+*/xX]|(?<=\\s)-");
        String[] opr = inp.split("[0-9]");
        int htg = Integer.parseInt(num[0]);
        String res = "";
        try {
            for (int i = 0; i < opr.length; i++) {
                switch (opr[i]) {
                    case "+":
                        htg += Integer.parseInt(num[i]);
                        break;
                    case "-":
                        htg -= Integer.parseInt(num[i]);
                        break;
                    case "*":
                        htg *= Integer.parseInt(num[i]);
                        break;
                    case "/":
                        htg /= Integer.parseInt(num[i]);
                        break;
                }
            }
            res = in + " = " + htg;
        } catch (Exception e) {
            res = "tidak bisa dilakukan";
        }
        System.out.println(res);
    }

}

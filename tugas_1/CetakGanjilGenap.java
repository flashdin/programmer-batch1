package file;

import java.util.*;

public class CetakGanjilGenap {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Enter A: ");
        int a = input.nextInt();
        System.out.print("Enter B: ");
        int b = input.nextInt();
        tukar(a, b);
    }

    private static void tukar(int a, int b) {
        for (int i = a; i <= b; i++) {
            String str = "ganjil";
            if (i % 2 == 0) {
                str = "genap";
            }
            System.out.println("Angka " + i + " adalah " + str);
        }
    }

}


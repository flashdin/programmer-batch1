package file;

import java.util.*;

public class HitungVokal {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Enter String: ");
        String in = input.nextLine();
        vokal(in);
        input.close();
    }

    private static void vokal(String in) {
        char[] chars = in.toLowerCase().toCharArray();
        int vowels = 0;
        String str = "";
        Set<Character> charSet = new LinkedHashSet<>();
        for (char c : chars) {
            charSet.add(c);
        }
        for (Character character : charSet) {
            if (character == 'a' || character == 'e' || character == 'i' || character == 'o' || character == 'u') {
                str += character + " dan ";
                vowels++;
            }
        }
        str = str.length() <= 0 ? "" : " yaitu " + str.substring(0, (str.length() - 5));
        System.out.println("'" + in + "'" + " = " + vowels + str);
    }

}


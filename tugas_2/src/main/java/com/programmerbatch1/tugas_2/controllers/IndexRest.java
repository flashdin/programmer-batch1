package com.programmerbatch1.tugas_2.controllers;

import com.programmerbatch1.tugas_2.models.CetakGanjilGenap;
import com.programmerbatch1.tugas_2.models.HitungVokal;
import com.programmerbatch1.tugas_2.models.KalkulatorSederhana;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api")
public class IndexRest {

    @Autowired
    private KalkulatorSederhana kalkulatorSederhana;
    @Autowired
    private CetakGanjilGenap cetakGanjilGenap;
    @Autowired
    private HitungVokal hitungVokal;

    @PostMapping("/kal")
    public String kalkulator(@RequestBody Map<String, Object> par) {
        return kalkulatorSederhana.kalkulator(par);
    }

    @PostMapping("/gg")
    public List<String> ganjilGenap(@RequestBody Map<String, Object> par) {
        return cetakGanjilGenap.tukar(Integer.parseInt(par.get("a").toString()), Integer.parseInt(par.get("b").toString()));
    }

    @PostMapping("/hv")
    public String hurufVokal(@RequestBody Map<String, Object> par) {
        return hitungVokal.vokal(par.get("in").toString());
    }

}

package com.programmerbatch1.tugas_2.controllers;

import com.programmerbatch1.tugas_2.components.StringURLSplit;
import com.programmerbatch1.tugas_2.models.CetakGanjilGenap;
import com.programmerbatch1.tugas_2.models.HitungVokal;
import com.programmerbatch1.tugas_2.models.KalkulatorSederhana;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
public class IndexController {


    @Autowired
    private KalkulatorSederhana kalkulatorSederhana;
    @Autowired
    private CetakGanjilGenap cetakGanjilGenap;
    @Autowired
    private HitungVokal hitungVokal;

    @RequestMapping("/")
    public String index(Map<String, Object> model) {
        model.put("title", "Tugas 2");
        return "index";
    }

    @RequestMapping("/kal")
    public String kalkulator(Map<String, Object> model, @RequestBody(required = false) String par) throws MalformedURLException {
        model.put("title", "Kalkulator");
        String res = "";
        if (par != null) {
            Map<String, Object> map = StringURLSplit.splitQuery(new URL("http://?" + par));
            model.put("txtA", map.get("txtA"));
            model.put("txtB", map.get("txtB"));
            res = kalkulatorSederhana.kalkulator(map);
        }
        model.put("res", res);
        return "kalkulator";
    }

    @RequestMapping("/gg")
    public String ganjilGenap(Map<String, Object> model, @RequestBody(required = false) String par) throws MalformedURLException {
        model.put("title", "Ganjil Genap");
        List<String> res = new ArrayList<>();
        if (par != null) {
            Map<String, Object> map = StringURLSplit.splitQuery(new URL("http://?" + par));
            model.put("a", map.get("a").toString());
            model.put("b", map.get("b").toString());
            res = cetakGanjilGenap.tukar(Integer.parseInt(map.get("a").toString()), Integer.parseInt(map.get("b").toString()));
        }
        model.put("res", res);
        return "ganjil_genap";
    }

    @RequestMapping("/hv")
    public String hurufVokal(Map<String, Object> model, @RequestBody(required = false) String par) throws MalformedURLException {
        model.put("title", "Huruf Vokal");
        String res = "";
        if (par != null) {
            Map<String, Object> map = StringURLSplit.splitQuery(new URL("http://?" + par));
            model.put("in", map.get("in").toString());
            res = hitungVokal.vokal(map.get("in").toString());
        }
        model.put("res", res);
        return "huruf_vokal";
    }

}

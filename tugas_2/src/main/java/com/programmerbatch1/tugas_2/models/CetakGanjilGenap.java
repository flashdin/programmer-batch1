package com.programmerbatch1.tugas_2.models;

import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class CetakGanjilGenap {

    public List<String> tukar(int a, int b) {
        List<String> ls = new ArrayList<>();
        for (int i = a; i <= b; i++) {
            String str = "ganjil";
            if (i % 2 == 0) {
                str = "genap";
            }
            ls.add("Angka " + i + " adalah " + str);
        }
        return ls;
    }

}


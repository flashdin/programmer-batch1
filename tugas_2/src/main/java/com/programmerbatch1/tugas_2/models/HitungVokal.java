package com.programmerbatch1.tugas_2.models;

import com.programmerbatch1.tugas_2.components.XSSFilter;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class HitungVokal {

    public String vokal(String inx) {
        String in = XSSFilter.stripXSS(inx);
        char[] chars = in.toLowerCase().toCharArray();
        int vowels = 0;
        String str = "";
        Set<Character> charSet = new LinkedHashSet<>();
        for (char c : chars) {
            charSet.add(c);
        }
        for (Character character : charSet) {
            if (character == 'a' || character == 'e' || character == 'i' || character == 'o' || character == 'u') {
                str += character + " dan ";
                vowels++;
            }
        }
        str = str.length() <= 0 ? "" : " yaitu " + str.substring(0, (str.length() - 5));
        return "'" + in + "'" + " = " + vowels + str;
    }

}


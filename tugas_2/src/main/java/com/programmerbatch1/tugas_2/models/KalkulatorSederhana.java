package com.programmerbatch1.tugas_2.models;

import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class KalkulatorSederhana {

    public String kalkulator(Map<String, Object> par) {
        String in = par.get("txtA").toString() + par.get("opr").toString() + par.get("txtB").toString();
        String inp = in.replaceAll(" ", "");
        String[] num = inp.split("[+*/xX\\-]|(?<=\\s)-");
        String[] opr = inp.split("[0-9]");
        int htg = Integer.parseInt(num[0]);
        String res = "";
        try {
            for (int i = 0; i < opr.length; i++) {
                switch (opr[i]) {
                    case "+":
                        htg += Integer.parseInt(num[i]);
                        break;
                    case "-":
                        htg -= Integer.parseInt(num[i]);
                        break;
                    case "*":
                        htg *= Integer.parseInt(num[i]);
                        break;
                    case "/":
                        htg /= Integer.parseInt(num[i]);
                        break;
                }
            }
            res = in + " = " + htg;
        } catch (Exception e) {
            res = "tidak bisa dilakukan";
        }
        return res;
    }

}

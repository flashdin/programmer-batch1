package com.flashdin.app_struktur_organisasi.services;

import com.flashdin.app_struktur_organisasi.pojos.Company;

public interface CompanyService extends BaseService<Company> {
}

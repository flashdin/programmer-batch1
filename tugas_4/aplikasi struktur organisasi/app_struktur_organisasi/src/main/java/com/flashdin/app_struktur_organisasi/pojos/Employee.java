package com.flashdin.app_struktur_organisasi.pojos;


import java.util.List;

public class Employee {

    private long id;
    private String nama;
    private long atasanId;
    private Company company;
    private List<Employee> employees;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }


    public long getAtasanId() {
        return atasanId;
    }

    public void setAtasanId(long atasanId) {
        this.atasanId = atasanId;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }
}

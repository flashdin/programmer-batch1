package com.flashdin.app_struktur_organisasi.models.impl;

import com.flashdin.app_struktur_organisasi.components.ExecFunction;
import com.flashdin.app_struktur_organisasi.models.EmployeeRepository;
import com.flashdin.app_struktur_organisasi.pojos.Company;
import com.flashdin.app_struktur_organisasi.pojos.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

@Repository
public class EmployeeRepositoryImpl implements EmployeeRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public List<Employee> findDataAll(Map<String, Object> par) {
        String query = "select * from employee e join company c on e.company_id=c.id";
        List<Employee> ls = jdbcTemplate.query(query, this.rowMapper());
        return ls;
    }

    @Override
    public Optional<Employee> findDataById(long id) {
        String query = "select * from employee e join company c on e.company_id=c.id"
                + " where e.id=?";
        Employee employee = jdbcTemplate.queryForObject(query, new Object[]{id}, this.rowMapper());
        Optional<Employee> employeeOptional = Optional.ofNullable(employee);
        return employeeOptional;
    }

    @Override
    public Map<String, Object> saveData(Employee par) {
        Map<String, Object> res = new HashMap<>();
        Map<String, Object> map = new HashMap<>();
        map.put("nama", par.getNama());
        map.put("atasan_id", par.getAtasanId() == 0 ? null : par.getAtasanId());
        map.put("company_id", par.getCompany().getId());
        ExecFunction execFunction = new ExecFunction(this.jdbcTemplate);
        if (par.getId() == 0) {
            String sql = "select coalesce(max(id), 0) as id from employee";
            Employee employee = jdbcTemplate.queryForObject(
                    sql, new BeanPropertyRowMapper<>(Employee.class));
            map.put("id", employee.getId() + 1);
            res = execFunction.execQueryInsert("employee", map, "Data");
        } else {
            Map<String, Object> mapWhere = new HashMap<>();
            mapWhere.put("id", par.getId());
            res = execFunction.execQueryUpdate("employee", map, mapWhere, "Data");
        }
        return res;
    }

    @Override
    public Map<String, Object> deleteData(Employee par) {
        Map<String, Object> map = new HashMap<>();
        map.put("id", par.getId());
        return new ExecFunction(this.jdbcTemplate).execQueryDelete("employee", map, "Data");
    }

    @Override
    public List<Employee> findDataByAtasanId(long id) {
        String query = "select * from employee e join company c on e.company_id=c.id where e.atasan_id=?";
        List<Employee> ls = jdbcTemplate.query(query, new Object[]{id}, this.rowMapper());
        return ls;
    }

    @Override
    public List<Employee> findAllCEO(Map<String, Object> par) {
        String query = "select *" +
                " from employee e join company c on e.company_id=c.id" +
                " where atasan_id is null";
        PreparedStatementSetter preparedStatementSetter = new PreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement preparedStatement) throws SQLException {
                if (par.get("companyId") != null) {
                    preparedStatement.setString(1, par.get("companyId").toString());
                }
            }
        };
        if (par.get("companyId") != null) {
            query += " and company_id=?";
        }
        List<Employee> ls = jdbcTemplate.query(query, preparedStatementSetter, this.rowMapper());
        return ls;
    }

    @Override
    public List<Employee> findAllDirektur() {
        String query = "select e.*,c.*" +
                " from employee e" +
                " join employee e2 on e.atasan_id = e2.id" +
                " join company c on e.company_id=c.id" +
                " where e2.atasan_id is null";
        List<Employee> ls = jdbcTemplate.query(query, this.rowMapper());
        return ls;
    }

    @Override
    public List<Employee> findAllManager() {
        String query = "select e.*,c.*" +
                " from employee e" +
                " join company c on e.company_id=c.id" +
                " where e.atasan_id in" +
                " (select e2.id" +
                " from employee e2" +
                " join employee e3 on e2.atasan_id = e3.id" +
                " where e3.atasan_id is null)" +
                " and e.id not in (select id" +
                " from employee" +
                " where id not in (select distinct atasan_id from employee where atasan_id is not null))";
        List<Employee> ls = jdbcTemplate.query(query, this.rowMapper());
        return ls;
    }

    @Override
    public List<Employee> findAllStaff() {
        String query = "select e.*, c.*" +
                " from employee e join company c on e.company_id=c.id" +
                " where e.id not in (select atasan_id from employee where atasan_id is not null)";
        List<Employee> ls = jdbcTemplate.query(query, this.rowMapper());
        return ls;
    }

    @Override
    public List<Employee> findAllNotStaff() {
        String query = "select *" +
                " from employee e join company c on e.company_id=c.id" +
                " where e.id in (select atasan_id from employee)";
        List<Employee> ls = jdbcTemplate.query(query, this.rowMapper());
        return ls;
    }

    @Override
    public List<Employee> findAllNotMe(long id) {
        String query = "select * from employee e join company c on e.company_id=c.id" +
                " where e.id !=?";
        List<Employee> ls = jdbcTemplate.query(query, new Object[]{id}, this.rowMapper());
        return ls;
    }

    private RowMapper<Employee> rowMapper() {
        return new RowMapper<Employee>() {
            @Override
            public Employee mapRow(ResultSet resultSet, int i) throws SQLException {
                Company company = new Company();
                company.setId(resultSet.getLong("c.id"));
                company.setNama(resultSet.getString("c.nama"));
                company.setAlamat(resultSet.getString("c.alamat"));
                Employee employee = new Employee();
                employee.setId(resultSet.getLong("e.id"));
                employee.setNama(resultSet.getString("e.nama"));
                employee.setAtasanId(resultSet.getLong("e.atasan_id"));
                employee.setCompany(company);
                employee.setEmployees(new ArrayList<>());
                List<Employee> employees = findDataByAtasanId(resultSet.getLong("e.id"));
                employee.setEmployees(employees);
                return employee;
            }
        };
    }
}

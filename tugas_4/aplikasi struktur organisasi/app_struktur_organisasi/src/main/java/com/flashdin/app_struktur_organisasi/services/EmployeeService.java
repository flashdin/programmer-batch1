package com.flashdin.app_struktur_organisasi.services;

import com.flashdin.app_struktur_organisasi.pojos.Employee;


public interface EmployeeService extends BaseService<Employee> {

}

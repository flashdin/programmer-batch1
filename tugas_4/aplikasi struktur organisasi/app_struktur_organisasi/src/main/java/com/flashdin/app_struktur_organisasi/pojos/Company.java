package com.flashdin.app_struktur_organisasi.pojos;


public class Company {

  private long id;
  private String nama;
  private String alamat;


  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }


  public String getNama() {
    return nama;
  }

  public void setNama(String nama) {
    this.nama = nama;
  }


  public String getAlamat() {
    return alamat;
  }

  public void setAlamat(String alamat) {
    this.alamat = alamat;
  }

}

package com.flashdin.app_struktur_organisasi.services.impl;

import com.flashdin.app_struktur_organisasi.models.EmployeeRepository;
import com.flashdin.app_struktur_organisasi.pojos.Employee;
import com.flashdin.app_struktur_organisasi.services.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;

    @Override
    public List<Employee> findDataAll(Map<String, Object> par) {
        List<Employee> employees = new ArrayList<>();
        if (par.get("jabatan") != null) {
            switch (par.get("jabatan").toString()) {
                case "notStaff":
                    employees = employeeRepository.findAllNotStaff();
                    break;
                case "ceo":
                    employees = employeeRepository.findAllCEO(par);
                    break;
                case "direktur":
                    employees = employeeRepository.findAllDirektur();
                    break;
                case "manager":
                    employees = employeeRepository.findAllManager();
                    break;
                default:
                    employees = employeeRepository.findDataAll(par);
                    break;
            }
        } else {
            employees = employeeRepository.findDataAll(par);
        }
        return employees;
    }

    @Override
    public Optional<Employee> findDataById(long id) {
        return employeeRepository.findDataById(id);
    }

    @Override
    public Map<String, Object> saveData(Employee par) {
        return employeeRepository.saveData(par);
    }

    @Override
    public Map<String, Object> deleteData(Employee par) {
        return employeeRepository.deleteData(par);
    }

}

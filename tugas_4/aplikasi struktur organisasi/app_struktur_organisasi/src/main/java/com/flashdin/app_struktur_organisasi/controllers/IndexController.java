package com.flashdin.app_struktur_organisasi.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.*;

@Controller
public class IndexController {

    @GetMapping(path = "/")
    public String index(Model model) {
        Map<String, Object> map = new HashMap<>();
        model.addAttribute("title", "App Struktur Organisasi");
        model.addAttribute("subtitle", "");
        model.addAttribute("filterPg", "");
        model.addAttribute("mainPg", "main");
        return "index";
    }

}

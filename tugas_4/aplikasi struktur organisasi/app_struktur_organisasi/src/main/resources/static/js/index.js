var API_BASE_URL = 'http://' + window.location.hostname + ':8090';
var API_IMG_URL = 'http://' + window.location.hostname + ':3002';

function imgErr() {
    // https://via.placeholder.com/720x480
    var img = document.getElementsByTagName('img');
    for (var i = 0; i < img.length; i++) {
        img[i].onerror = function (e) {
            e.target.src = API_BASE_URL + '/img/404.png';
        }
    }
}
package com.flashdin.region.pojos;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "kab_kot", schema = "region")
@Getter
@Setter
public class KabKot {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "int(11)")
    private long id;
    @Column(columnDefinition = "varchar(50)")
    private String nama;

    @OneToMany(mappedBy = "kabKot", cascade = CascadeType.ALL, targetEntity = Kecamatan.class)
    private List<Kecamatan> kecamatans;
//    @JsonBackReference
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "propinsiId", referencedColumnName = "id", nullable = true)
    private Propinsi propinsi;
}

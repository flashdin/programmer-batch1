package com.flashdin.region.controllers;

import com.flashdin.region.pojos.Propinsi;
import com.flashdin.region.services.PropinsiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Controller
@RequestMapping("/propinsi")
public class PropinsiController {

    @Autowired
    @Qualifier("propinsiServiceImpl")
    private PropinsiService propinsiService;

    @GetMapping(path = "")
    public ModelAndView viewList(Model model, @RequestParam Map<String, Object> par) {
        ModelAndView mav = new ModelAndView("index");
        mav.addObject("title", "App Region");
        mav.addObject("subtitle", "");
        mav.addObject("filterPg", "");
        Map<String, Object> map = par;
        List<Propinsi> propinsis = propinsiService.findDataAll(map);
        mav.addObject("data", propinsis);
        mav.addObject("res", model.getAttribute("res"));
        mav.addObject("mainPg", "propinsi/list");
        return mav;
    }

    @GetMapping(path = "/{id}")
    public ModelAndView viewFormAdd(@PathVariable(value = "id") long id) {
        ModelAndView mav = new ModelAndView("layout/content");
        mav.addObject("title", "App Region");
        mav.addObject("subtitle", "");
        mav.addObject("filterPg", "");
        mav.addObject("propinsi", new Propinsi());
        if (id > 0) {
            Optional<Propinsi> propinsis = propinsiService.findDataById(id);
            if (propinsis.isPresent()) {
                mav.addObject("propinsi", propinsis.get());
            }
        }
        mav.addObject("mainPg", "propinsi/add");
        return mav;
    }

    @PostMapping(path = "")
    public RedirectView saveData(@ModelAttribute Propinsi par, BindingResult bindingResult,
                                 RedirectAttributes redirectAttributes) {
        Map<String, Object> map = propinsiService.saveData(par);
        RedirectView redirectView = new RedirectView();
        redirectView.setContextRelative(true);
        redirectView.setUrl("/propinsi");
        redirectAttributes.addFlashAttribute("res", map);
        return redirectView;
    }

    @PostMapping(path = "/del")
    public String deleteData(@ModelAttribute Propinsi par, RedirectAttributes redirectAttributes) {
        Map<String, Object> map = propinsiService.deleteData(par);
        redirectAttributes.addFlashAttribute("res", map);
        return "redirect:/propinsi";
    }

}

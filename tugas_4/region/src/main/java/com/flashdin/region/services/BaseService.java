package com.flashdin.region.services;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface BaseService<T> {

    List<T> findDataAll(Map<String, Object> par);
    Optional<T> findDataById(long id);
    Map<String, Object> saveData(T par);
    Map<String, Object> deleteData(T par);
}

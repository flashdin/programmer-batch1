package com.flashdin.region.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.HashMap;
import java.util.Map;

@Controller
public class IndexController {

    @GetMapping(path = "/")
    public String index(Model model) {
        Map<String, Object> map = new HashMap<>();
        model.addAttribute("title", "App Region");
        model.addAttribute("subtitle", "");
        model.addAttribute("filterPg", "");
        model.addAttribute("mainPg", "main");
        return "index";
    }

}

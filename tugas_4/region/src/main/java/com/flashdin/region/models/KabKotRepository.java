package com.flashdin.region.models;

import com.flashdin.region.pojos.KabKot;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface KabKotRepository extends JpaRepository<KabKot, Long>, JpaSpecificationExecutor<KabKot> {
}

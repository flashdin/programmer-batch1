package com.flashdin.region.models;

import com.flashdin.region.pojos.Kecamatan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface KecamatanRepository extends JpaRepository<Kecamatan, Long>, JpaSpecificationExecutor<Kecamatan> {
}

package com.flashdin.region.controllers;

import com.flashdin.region.pojos.Propinsi;
import com.flashdin.region.services.PropinsiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/propinsi")
public class PropinsiRest {

    @Autowired
    @Qualifier("propinsiServiceImpl")
    private PropinsiService propinsiService;

    @GetMapping(path = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Propinsi> findDataAll(@RequestParam Map<String, Object> par) {
        return propinsiService.findDataAll(par);
    }


}

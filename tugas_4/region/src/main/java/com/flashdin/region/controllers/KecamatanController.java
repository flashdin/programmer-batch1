package com.flashdin.region.controllers;

import com.flashdin.region.pojos.Kecamatan;
import com.flashdin.region.pojos.Propinsi;
import com.flashdin.region.services.KabKotService;
import com.flashdin.region.services.KecamatanService;
import com.flashdin.region.services.PropinsiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.*;

@Controller
@RequestMapping("/kecamatan")
public class KecamatanController {

    @Autowired
    @Qualifier("propinsiServiceImpl")
    private PropinsiService propinsiService;
    @Autowired
    @Qualifier("kabKotServiceImpl")
    private KabKotService kabKotService;
    @Autowired
    @Qualifier("kecamatanServiceImpl")
    private KecamatanService kecamatanService;

    @GetMapping(path = "")
    public ModelAndView viewList(Model model, @RequestParam Map<String, Object> par) {
        ModelAndView mav = new ModelAndView("index");
        mav.addObject("title", "App Region");
        mav.addObject("subtitle", "");
        mav.addObject("filterPg", "");
        Map<String, Object> map = par;
        List<Kecamatan> kecamatans = kecamatanService.findDataAll(map);
        mav.addObject("data", kecamatans);
        mav.addObject("res", model.getAttribute("res"));
        mav.addObject("mainPg", "kecamatan/list");
        return mav;
    }

    @GetMapping(path = "/{id}")
    public ModelAndView viewFormAdd(@PathVariable(value = "id") long id) {
        ModelAndView mav = new ModelAndView("layout/content");
        mav.addObject("title", "App Region");
        mav.addObject("subtitle", "");
        mav.addObject("filterPg", "");
        Map<String, Object> map = new HashMap<>();
        mav.addObject("propinsiOpt", propinsiService.findDataAll(map));
        map = new HashMap<>();
        mav.addObject("kecamatan", new Kecamatan());
        if (id > 0) {
            Optional<Kecamatan> companies = kecamatanService.findDataById(id);
            if (companies.isPresent()) {
                mav.addObject("kecamatan", companies.get());
            }
        }
        mav.addObject("mainPg", "kecamatan/add");
        return mav;
    }

    @PostMapping(path = "")
    public RedirectView saveData(@ModelAttribute Kecamatan par, BindingResult bindingResult,
                                 RedirectAttributes redirectAttributes) {
        Map<String, Object> map = kecamatanService.saveData(par);
        RedirectView redirectView = new RedirectView();
        redirectView.setContextRelative(true);
        redirectView.setUrl("/kecamatan");
        redirectAttributes.addFlashAttribute("res", map);
        return redirectView;
    }

    @PostMapping(path = "/del")
    public String deleteData(@ModelAttribute Kecamatan par, RedirectAttributes redirectAttributes) {
        Map<String, Object> map = kecamatanService.deleteData(par);
        redirectAttributes.addFlashAttribute("res", map);
        return "redirect:/kecamatan";
    }

}

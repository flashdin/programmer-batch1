package com.flashdin.region.pojos;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "propinsi", schema = "region")
@Getter
@Setter
public class Propinsi {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "int(11)")
    private long id;
    @Column(columnDefinition = "varchar(50)")
    private String nama;

//    @JsonManagedReference
    @OneToMany(mappedBy = "propinsi", cascade = CascadeType.ALL, targetEntity = KabKot.class)
    private List<KabKot> kabKots;

}

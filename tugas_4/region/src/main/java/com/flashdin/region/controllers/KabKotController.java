package com.flashdin.region.controllers;

import com.flashdin.region.pojos.KabKot;
import com.flashdin.region.services.KabKotService;
import com.flashdin.region.services.PropinsiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Controller
@RequestMapping("/kabkot")
public class KabKotController {

    @Autowired
    @Qualifier("propinsiServiceImpl")
    private PropinsiService propinsiService;
    @Autowired
    @Qualifier("kabKotServiceImpl")
    private KabKotService kabKotService;

    @GetMapping(path = "")
    public ModelAndView viewList(Model model, @RequestParam Map<String, Object> par) {
        ModelAndView mav = new ModelAndView("index");
        mav.addObject("title", "App Region");
        mav.addObject("subtitle", "");
        mav.addObject("filterPg", "");
        Map<String, Object> map = par;
        List<KabKot> kabKots = kabKotService.findDataAll(map);
        mav.addObject("data", kabKots);
        mav.addObject("res", model.getAttribute("res"));
        mav.addObject("mainPg", "kabkot/list");
        return mav;
    }

    @GetMapping(path = "/{id}")
    public ModelAndView viewFormAdd(@PathVariable(value = "id") long id) {
        ModelAndView mav = new ModelAndView("layout/content");
        mav.addObject("title", "App Region");
        mav.addObject("subtitle", "");
        mav.addObject("filterPg", "");
        Map<String, Object> map = new HashMap<>();
        mav.addObject("propinsiOpt", propinsiService.findDataAll(map));
        mav.addObject("kabKot", new KabKot());
        if (id > 0) {
            Optional<KabKot> kabKots = kabKotService.findDataById(id);
            if (kabKots.isPresent()) {
                mav.addObject("kabKot", kabKots.get());
            }
        }
        mav.addObject("mainPg", "kabkot/add");
        return mav;
    }

    @PostMapping(path = "")
    public RedirectView saveData(@ModelAttribute KabKot par, BindingResult bindingResult,
                                 RedirectAttributes redirectAttributes) {
        Map<String, Object> map = kabKotService.saveData(par);
        RedirectView redirectView = new RedirectView();
        redirectView.setContextRelative(true);
        redirectView.setUrl("/kabkot");
        redirectAttributes.addFlashAttribute("res", map);
        return redirectView;
    }

    @PostMapping(path = "/del")
    public String deleteData(@ModelAttribute KabKot par, RedirectAttributes redirectAttributes) {
        Map<String, Object> map = kabKotService.deleteData(par);
        redirectAttributes.addFlashAttribute("res", map);
        return "redirect:/kabkot";
    }

}

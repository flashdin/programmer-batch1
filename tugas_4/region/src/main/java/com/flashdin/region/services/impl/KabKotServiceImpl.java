package com.flashdin.region.services.impl;

import com.flashdin.region.models.KabKotRepository;
import com.flashdin.region.pojos.KabKot;
import com.flashdin.region.services.KabKotService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class KabKotServiceImpl implements KabKotService {

    @Autowired
    private KabKotRepository kabKotRepository;

    @Override
    public List<KabKot> findDataAll(Map<String, Object> par) {
        return kabKotRepository.findAll();
    }

    @Override
    public Optional<KabKot> findDataById(long id) {
        return kabKotRepository.findById(id);
    }

    @Override
    public Map<String, Object> saveData(KabKot par) {
        Map<String, Object> map = new HashMap<>();
        KabKot kabKot = kabKotRepository.save(par);
        if (kabKot.getId() > 0) {
            map.put("out", "1");
            map.put("msg", "Data berhasil disimpan");
        } else {
            map.put("out", "0");
            map.put("msg", "Data gagal disimpan");
        }
        return map;
    }

    @Override
    public Map<String, Object> deleteData(KabKot par) {
        Map<String, Object> map = new HashMap<>();
        kabKotRepository.delete(par);
        long cn = kabKotRepository.count(Example.of(par));
        if (cn <= 0) {
            map.put("out", "1");
            map.put("msg", "Data berhasil dihapus");
        } else {
            map.put("out", "0");
            map.put("msg", "Data gagal dihapus");
        }
        return map;
    }
}

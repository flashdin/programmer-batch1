package com.flashdin.region.models;

import com.flashdin.region.pojos.Propinsi;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface PropinsiRepository extends JpaRepository<Propinsi, Long>, JpaSpecificationExecutor<Propinsi> {
}

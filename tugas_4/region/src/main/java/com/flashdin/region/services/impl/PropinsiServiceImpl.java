package com.flashdin.region.services.impl;

import com.flashdin.region.models.PropinsiRepository;
import com.flashdin.region.pojos.Propinsi;
import com.flashdin.region.services.PropinsiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class PropinsiServiceImpl implements PropinsiService {

    @Autowired
    private PropinsiRepository propinsiRepository;

    @Override
    public List<Propinsi> findDataAll(Map<String, Object> par) {
        return propinsiRepository.findAll();
    }

    @Override
    public Optional<Propinsi> findDataById(long id) {
        return propinsiRepository.findById(id);
    }

    @Override
    public Map<String, Object> saveData(Propinsi par) {
        Map<String, Object> map = new HashMap<>();
        Propinsi propinsi = propinsiRepository.save(par);
        if (propinsi.getId() > 0) {
            map.put("out", "1");
            map.put("msg", "Data berhasil disimpan");
        } else {
            map.put("out", "0");
            map.put("msg", "Data gagal disimpan");
        }
        return map;
    }

    @Override
    public Map<String, Object> deleteData(Propinsi par) {
        Map<String, Object> map = new HashMap<>();
        propinsiRepository.delete(par);
        long cn = propinsiRepository.count(Example.of(par));
        if (cn <= 0) {
            map.put("out", "1");
            map.put("msg", "Data berhasil dihapus");
        } else {
            map.put("out", "0");
            map.put("msg", "Data gagal dihapus");
        }
        return map;
    }
}

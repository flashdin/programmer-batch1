package com.flashdin.region.pojos;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "kecamatan", schema = "region")
@Getter
@Setter
public class Kecamatan {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "int(11)")
    private long id;
    @Column(columnDefinition = "varchar(50)")
    private String nama;
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "kabKotId", referencedColumnName = "id", nullable = true)
    private KabKot kabKot;

}

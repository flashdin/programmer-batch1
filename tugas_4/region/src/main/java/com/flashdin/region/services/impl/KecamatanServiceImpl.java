package com.flashdin.region.services.impl;

import com.flashdin.region.models.KecamatanRepository;
import com.flashdin.region.pojos.Kecamatan;
import com.flashdin.region.services.KecamatanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class KecamatanServiceImpl implements KecamatanService {

    @Autowired
    private KecamatanRepository kecamatanRepository;

    @Override
    public List<Kecamatan> findDataAll(Map<String, Object> par) {
        return kecamatanRepository.findAll();
    }

    @Override
    public Optional<Kecamatan> findDataById(long id) {
        return kecamatanRepository.findById(id);
    }

    @Override
    public Map<String, Object> saveData(Kecamatan par) {
        Map<String, Object> map = new HashMap<>();
        Kecamatan kecamatan = kecamatanRepository.save(par);
        if (kecamatan.getId() > 0) {
            map.put("out", "1");
            map.put("msg", "Data berhasil disimpan");
        } else {
            map.put("out", "0");
            map.put("msg", "Data gagal disimpan");
        }
        return map;
    }

    @Override
    public Map<String, Object> deleteData(Kecamatan par) {
        Map<String, Object> map = new HashMap<>();
        kecamatanRepository.delete(par);
        long cn = kecamatanRepository.count(Example.of(par));
        if (cn <= 0) {
            map.put("out", "1");
            map.put("msg", "Data berhasil dihapus");
        } else {
            map.put("out", "0");
            map.put("msg", "Data gagal dihapus");
        }
        return map;
    }
}

package com.flashdin.bioskop.controllers;

import com.flashdin.bioskop.pojos.Penonton;
import com.flashdin.bioskop.services.PenontonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping(path = "/api/penonton")
public class PenontonRest {

    @Autowired
    @Qualifier("penontonServiceImpl")
    private PenontonService penontonService;

    @GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Optional<Penonton> findDataById(@PathVariable(name = "id") long id) {
        return penontonService.findDataById(id);
    }

}

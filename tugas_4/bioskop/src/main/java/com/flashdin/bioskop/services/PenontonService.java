package com.flashdin.bioskop.services;

import com.flashdin.bioskop.pojos.Penonton;

public interface PenontonService extends BaseService<Penonton> {

    long countById(long id);

}

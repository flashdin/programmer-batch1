package com.flashdin.bioskop.services.impl;

import com.flashdin.bioskop.models.PenontonRepository;
import com.flashdin.bioskop.pojos.Penonton;
import com.flashdin.bioskop.services.PenontonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class PenontonServiceImpl implements PenontonService {

    @Autowired
    private PenontonRepository penontonRepository;

    @Override
    public List<Penonton> findDataAll(Map<String, Object> par) {
        return penontonRepository.findAll();
    }

    @Override
    public Optional<Penonton> findDataById(long id) {
        return penontonRepository.findById(id);
    }

    @Override
    public Map<String, Object> saveData(Penonton par) {
        Map<String, Object> map = new HashMap<>();
        Penonton penonton = penontonRepository.save(par);
        if (penonton.getId() > 0) {
            map.put("out", "1");
            map.put("msg", "Data berhasil disimpan");
        } else {
            map.put("out", "0");
            map.put("msg", "Data gagal disimpan");
        }
        return map;
    }

    @Override
    public Map<String, Object> deleteData(Penonton par) {
        Map<String, Object> map = new HashMap<>();
        penontonRepository.delete(par);
        long cn = penontonRepository.count(Example.of(par));
        if (cn <= 0) {
            map.put("out", "1");
            map.put("msg", "Data berhasil dihapus");
        } else {
            map.put("out", "0");
            map.put("msg", "Data gagal dihapus");
        }
        return map;
    }

    @Override
    public long countById(long id) {
        Penonton penonton = new Penonton();
        penonton.setId(id);
        return penontonRepository.count(Example.of(penonton));
    }
}

package com.flashdin.bioskop.services.impl;

import com.flashdin.bioskop.models.FilmRepository;
import com.flashdin.bioskop.pojos.Film;
import com.flashdin.bioskop.services.FilmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class FilmServiceImpl implements FilmService {

    @Autowired
    private FilmRepository filmRepository;

    @Override
    public List<Film> findDataAll(Map<String, Object> par) {
        return filmRepository.findAll();
    }

    @Override
    public Optional<Film> findDataById(long id) {
        return filmRepository.findById(id);
    }

    @Override
    public Map<String, Object> saveData(Film par) {
        Map<String, Object> map = new HashMap<>();
        Film film = filmRepository.save(par);
        if (film.getId() > 0) {
            map.put("out", "1");
            map.put("msg", "Data berhasil disimpan");
        } else {
            map.put("out", "0");
            map.put("msg", "Data gagal disimpan");
        }
        return map;
    }

    @Override
    public Map<String, Object> deleteData(Film par) {
        Map<String, Object> map = new HashMap<>();
        filmRepository.delete(par);
        long cn = filmRepository.count(Example.of(par));
        if (cn <= 0) {
            map.put("out", "1");
            map.put("msg", "Data berhasil dihapus");
        } else {
            map.put("out", "0");
            map.put("msg", "Data gagal dihapus");
        }
        return map;
    }
}

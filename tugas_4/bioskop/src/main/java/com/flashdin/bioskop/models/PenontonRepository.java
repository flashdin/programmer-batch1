package com.flashdin.bioskop.models;

import com.flashdin.bioskop.pojos.Penonton;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface PenontonRepository extends JpaRepository<Penonton, Long>, JpaSpecificationExecutor<Penonton> {

}

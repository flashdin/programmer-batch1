package com.flashdin.bioskop.controllers;

import com.flashdin.bioskop.pojos.Film;
import com.flashdin.bioskop.services.FilmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Controller
@RequestMapping("/film")
public class FilmController {

    @Autowired
    @Qualifier("filmServiceImpl")
    private FilmService filmService;

    @GetMapping(path = "")
    public ModelAndView viewList(Model model, @RequestParam Map<String, Object> par) {
        ModelAndView mav = new ModelAndView("index");
        mav.addObject("title", "App Bioskop");
        mav.addObject("subtitle", "");
        mav.addObject("filterPg", "");
        Map<String, Object> map = par;
        List<Film> films = filmService.findDataAll(map);
        mav.addObject("data", films);
        mav.addObject("res", model.getAttribute("res"));
        mav.addObject("mainPg", "film/list");
        return mav;
    }

    @GetMapping(path = "/{id}")
    public ModelAndView viewFormAdd(@PathVariable(value = "id") long id) {
        ModelAndView mav = new ModelAndView("layout/content");
        mav.addObject("title", "App Bioskop");
        mav.addObject("subtitle", "");
        mav.addObject("filterPg", "");
        Film film = new Film();
        film.setWaktuTayang(new Date());
        mav.addObject("film", film);
        if (id > 0) {
            Optional<Film> films = filmService.findDataById(id);
            if (films.isPresent()) {
                mav.addObject("film", films.get());
            }
        }
        mav.addObject("mainPg", "film/add");
        return mav;
    }

    @PostMapping(path = "")
    public RedirectView saveData(@ModelAttribute Film par, BindingResult bindingResult,
                                 RedirectAttributes redirectAttributes) {
        Map<String, Object> map = filmService.saveData(par);
        RedirectView redirectView = new RedirectView();
        redirectView.setContextRelative(true);
        redirectView.setUrl("/film");
        redirectAttributes.addFlashAttribute("res", map);
        return redirectView;
    }

    @PostMapping(path = "/del")
    public String deleteData(@ModelAttribute Film par, RedirectAttributes redirectAttributes) {
        Map<String, Object> map = filmService.deleteData(par);
        redirectAttributes.addFlashAttribute("res", map);
        return "redirect:/film";
    }

}

package com.flashdin.bioskop.controllers;

import com.flashdin.bioskop.pojos.Penonton;
import com.flashdin.bioskop.services.FilmService;
import com.flashdin.bioskop.services.PenontonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import java.util.*;

@Controller
@RequestMapping("/penonton")
public class PenontonController {

    @Autowired
    @Qualifier("penontonServiceImpl")
    private PenontonService penontonService;
    @Autowired
    @Qualifier("filmServiceImpl")
    private FilmService filmService;

    @GetMapping(path = "")
    public ModelAndView viewList(Model model, @RequestParam Map<String, Object> par) {
        ModelAndView mav = new ModelAndView("index");
        mav.addObject("title", "App Bioskop");
        mav.addObject("subtitle", "");
        mav.addObject("filterPg", "");
        Map<String, Object> map = par;
        List<Penonton> penontons = penontonService.findDataAll(map);
        mav.addObject("data", penontons);
        mav.addObject("res", model.getAttribute("res"));
        mav.addObject("mainPg", "penonton/list");
        return mav;
    }

    @GetMapping(path = "/{id}")
    public ModelAndView viewFormAdd(@PathVariable(value = "id") long id) {
        ModelAndView mav = new ModelAndView("layout/content");
        mav.addObject("title", "App Bioskop");
        mav.addObject("subtitle", "");
        mav.addObject("filterPg", "");
        mav.addObject("penonton", new Penonton());
        if (id > 0) {
            Optional<Penonton> penontons = penontonService.findDataById(id);
            if (penontons.isPresent()) {
                mav.addObject("penonton", penontons.get());
            }
        }
        mav.addObject("mainPg", "penonton/add");
        return mav;
    }

    @PostMapping(path = "")
    public RedirectView saveData(@ModelAttribute Penonton par, BindingResult bindingResult,
                                 RedirectAttributes redirectAttributes) {
        Map<String, Object> map = penontonService.saveData(par);
        RedirectView redirectView = new RedirectView();
        redirectView.setContextRelative(true);
        redirectView.setUrl("/penonton");
        redirectAttributes.addFlashAttribute("res", map);
        return redirectView;
    }

    @PostMapping(path = "/del")
    public String deleteData(@ModelAttribute Penonton par, RedirectAttributes redirectAttributes) {
        Map<String, Object> map = penontonService.deleteData(par);
        redirectAttributes.addFlashAttribute("res", map);
        return "redirect:/penonton";
    }

    @GetMapping(path = "/ticket")
    public ModelAndView viewListTicket(Model model, @RequestParam Map<String, Object> par) {
        ModelAndView mav = new ModelAndView("layout/content");
        mav.addObject("title", "App Bioskop");
        mav.addObject("subtitle", "");
        mav.addObject("filterPg", "");
        mav.addObject("res", model.getAttribute("res"));
        mav.addObject("mainPg", "penonton/ticket");
        return mav;
    }

    @GetMapping(path = "/ticket/{id}")
    public ModelAndView viewFormTicket(@PathVariable(value = "id") long id) {
        ModelAndView mav = new ModelAndView("layout/content");
        mav.addObject("title", "App Bioskop");
        mav.addObject("subtitle", "");
        mav.addObject("filterPg", "");
        Penonton penonton = new Penonton();
        penonton.setFilms(filmService.findDataAll(new HashMap<>()));
        mav.addObject("penonton", penonton);
        if (id > 0) {
            Optional<Penonton> penontons = penontonService.findDataById(id);
            if (penontons.isPresent()) {
                mav.addObject("penonton", penontons.get());
            }
        }
        mav.addObject("mainPg", "film/ticket");
        return mav;
    }

    @PostMapping(path = "/ticket")
    public RedirectView saveDataTicket(@ModelAttribute Penonton par, BindingResult bindingResult,
                                 RedirectAttributes redirectAttributes) {
        Map<String, Object> map = penontonService.saveData(par);
        RedirectView redirectView = new RedirectView();
        redirectView.setContextRelative(true);
        redirectView.setUrl("/penonton");
        redirectAttributes.addFlashAttribute("res", map);
        return redirectView;
    }

}

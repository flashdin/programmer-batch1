package com.flashdin.bioskop.pojos;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "penonton", schema = "bioskop")
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
@Getter
@Setter
public class Penonton {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "int(11)")
    private long id;
    @Column(columnDefinition = "char(13)", unique = true)
    private String noId;
    @Column(columnDefinition = "varchar(100)")
    private String nama;
    @ManyToMany(targetEntity = Film.class, cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinTable(
            name = "penontonFilm",
            joinColumns = @JoinColumn(name = "penontonId", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "filmId", referencedColumnName = "id"))
    private List<Film> films;

}

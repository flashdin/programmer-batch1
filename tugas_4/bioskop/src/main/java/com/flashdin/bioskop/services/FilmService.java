package com.flashdin.bioskop.services;

import com.flashdin.bioskop.pojos.Film;

public interface FilmService extends BaseService<Film> {
}
